// Токен можно куда-либо сохранить, например в localStorage, если код исполняется в браузере
let token = window.localStorage.getItem('token')
const username = 'USERNAME'
const password = 'PASSWORD'
const baseUrl = 'https://online.sigmasms.ru/api'

const TOKEN_SAFE_ZONE = 5 * 60

// Если токен найден в localStorage - достаем его и в случае, если он устарел - сбрасываем
if (token) {
  let parsedToken = getTokenData(token)
  if (Date.now() / 1000 > parsedToken.exp - TOKEN_SAFE_ZONE) {
    token = undefined
    window.localStorage.removeItem('token')
  }
}

// Парсим данные токена, среднюю часть
function getTokenData(token) {
  const m = ('' + token).match(/.*?\.(.+)\..*?/)
  return JSON.parse(atob(m[1]))
}

async function getToken() {
  if (typeof token === 'string') {
    return token
  }

  // Если token === null значит какая-то функция getToken уже пытается достать токен из сервера
  if (token === null) {
    let timePassed = 0
    // Ожидаем, пока токен не будет получен или пока не пройдет 30 секунд
    while(token === null || timePassed >= 30) {
      await new Promise(resolve => setTimeout(resolve, 1000))
      timePassed += 1
    }

    // Если токен найден, возвращаем его
    if (typeof token === 'string') {
      return token
    }
  }
  // Блокируем токен и делаем запрос
  token = null

  const resp = await fetch(`${baseUrl}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username,
      password
    })
  }).then(response => response.json())

  token = resp.token
  window.localStorage.setItem('token', token)
  parsedToken = getTokenData(token)
  
  setTimeout(getToken, (parsedToken.exp * 1000) - Date.now() - (TOKEN_SAFE_ZONE * 1000))

  return token
}

async function sendSms(userPhone) {
  const token = await getToken()

  const resp = await fetch(`${baseUrl}/sendings`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: JSON.stringify({
      recipient: userPhone,
			type: "sms",
			payload: {
				// убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
				sender: "B-Media",
				text: "Это тестовое сообщение.\nИзменённый текст будет отправлен на модерацию."
			}
    })
  }).then(response => response.json())

  console.log(resp)
}