const fs = require('fs')
const fetch = require('node-fetch') // Вы можете использовать любую другую функцию HTTP запросов

// Можете использовать любую другую функцию декодинга Base64
const Base64Decode = (string) => {
  let buff = new Buffer(string, 'base64')
  return buff.toString('ascii')
}

// Замените следующие 3 функции чтобы записывать токен не файл, а в другое место
const tokenFilePath = __dirname + '/token.txt'
function loadToken() {
  try {
    fs.accessSync(tokenFilePath)
    return fs.readFileSync(tokenFilePath)
  } catch (error) {
    return
  }
}
function saveToken(token) {
  try {
    fs.accessSync(tokenFilePath)
    fs.writeFileSync(tokenFilePath, token, {
      encoding: 'utf8'
    })
  } catch (error) {
    return
  }
}
function removeSavedToken() {
  try {
    fs.accessSync(tokenFilePath)
    fs.unlinkSync(tokenFilePath)
  } catch (error) {
    return
  }
}

// Загружаем сохраненный токен
let token = loadToken()
const username = 'USERNAME'
const password = 'PASSWORD'
const baseUrl = 'https://online.sigmasms.ru/api'

const TOKEN_SAFE_ZONE = 5 * 60

// Проверяем его, и если срок его действия истек - сбрасываем и удаляем сохраненный токен
if (token) {
  let parsedToken = getTokenData(token)
  if (Date.now() / 1000 > parsedToken.exp - TOKEN_SAFE_ZONE) {
    token = undefined
    removeSavedToken()
  }
}

// Парсим данные токена, среднюю часть
function getTokenData(token) {
  const m = ('' + token).match(/.*?\.(.+)\..*?/)
  return JSON.parse(Base64Decode(m[1]))
}

async function getToken() {
  if (typeof token === 'string') {
    return token
  }

  // Если token === null значит какая-то функция getToken уже пытается достать токен из сервера
  if (token === null) {
    let timePassed = 0
    // Ожидаем, пока токен не будет получен или пока не пройдет 30 секунд
    while(token === null || timePassed >= 30) {
      await new Promise(resolve => setTimeout(resolve, 1000))
      timePassed += 1
    }

    // Если токен найден, возвращаем его
    if (typeof token === 'string') {
      return token
    }
  }
  // Блокируем токен и делаем запрос
  token = null

  const resp = await fetch(`${baseUrl}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username,
      password
    })
  }).then(response => response.json())

  token = resp.token
  saveToken()
  parsedToken = getTokenData(token)
  
  setTimeout(getToken, (parsedToken.exp * 1000) - Date.now() - (TOKEN_SAFE_ZONE * 1000))

  return token
}

async function sendSms(userPhone) {
  const token = await getToken()

  const resp = await fetch(`${baseUrl}/sendings`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    body: JSON.stringify({
      recipient: userPhone,
			type: "sms",
			payload: {
				// убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
				sender: "B-Media",
				text: "Это тестовое сообщение.\nИзменённый текст будет отправлен на модерацию."
			}
    })
  }).then(response => response.json())

  console.log(resp)
}