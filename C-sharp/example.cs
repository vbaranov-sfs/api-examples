using System.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    /// <summary> Состояния отправки сообщений</summary>
    public enum MessageStatus
    {
        /// <summary> ожидает отправки </summary>
        pending,
        /// <summary> приостановлено </summary>
        paused,
        /// <summary> в обработке </summary>
        processing,
        /// <summary> отправлено </summary>
        sent,
        /// <summary> доставлено </summary>
        delivered,
        /// <summary> просмотрено </summary>
        seen,
        /// <summary> ошибка при обработке/отправке сообщения </summary>
        failed,

        False
    }

    class Program
    {
        static void Main(string[] args)
        {
            // получаем авторизационный токен
            var token = GetToken().Result;

            // проверяем токен перед использованием
            var isExpired = IsTokenIExpired(token);
            if (isExpired) return;

            // отправляем sms на 1 номер
            var messageGuid = SendSms(token).Result;
            if (messageGuid == null) return;
            // проверяем статус сообщения
            var status = GetStatus(token, messageGuid.Value).Result;


            // отправляем картинку в хранилище
            var imageId = SendFile(token).Result;
            if (imageId == null) return;

            // Отправляем сообщение через viber на 2 номера
            // ставим в расписание через 10 мин
            // пытаемся переотправить через 60 сек
            var groupId = SendViberBulk(token, imageId.Value).Result;


            // Берем информацию о групповой рассылке
            // фильтруя по groupId
            //
            // ВАЖНО: если задача рассылки запланирована в расписании, то 
            // информация о статусе сообщений появится только после выполнения задачи
            if (groupId == null) return;
            var groupResult = GetGroupStatus(token, groupId.Value).Result;
        }

        private static async Task<string> GetToken()
        {
            HttpClient client = null;
            try
            {
                var content = JsonConvert.SerializeObject(new
                {
                    username = "username",
                    password = "password"
                });
                var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/login");
                request.Content = stringContent;

                client = new HttpClient();
                var response = await client.SendAsync(request);
                var responseContent = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dynamic result = JsonConvert.DeserializeObject(responseContent);
                    return (string) result.token;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                // log
            }
            return null;
        }

        private static bool IsTokenIExpired(string token)
        {
            var jwt = new JwtSecurityToken(token);
            return jwt.ValidTo < DateTime.UtcNow;
        }

        private static async Task<Guid?> SendSms(string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/sendings");
            request.Headers.Add("Authorization", token);
            var content = JsonConvert.SerializeObject(new
            {
                recipient = "+79999999999",
                type = "sms",
                payload = new
                {
                    // убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
                    sender = "B-Media",
                    text = "Hello world!"
                }
            });
            request.Content = new StringContent(content, Encoding.UTF8, "application/json");

            HttpClient client = null;
            try
            {
                client = new HttpClient();
                var response = await client.SendAsync(request);
                var resonseContent = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeAnonymousType(resonseContent, new
                {   // минимальный набор параметров для получения ответа
                    Id = default(Guid?),
                    Recipient = default(string),
                    Status = default(string),
                    Error = default(string)
                });

                if (response.StatusCode == System.Net.HttpStatusCode.OK && 
                    result.Id != null)
                {
                    return result.Id;
                }
                else
                {
                    // log
                }
            }
            catch (Exception e)
            {
                // log
            }
            finally
            {
                client?.Dispose();
            }
            return null;
        }

        private static async Task<MessageStatus> GetStatus(string token, Guid messageId)
        {
            var url = new Uri("https://online.sigmasms.ru/api/sendings/");
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url, messageId.ToString()));
            request.Headers.Add("Authorization", token);

            HttpClient client = null;
            try
            {
                client = new HttpClient();
                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeAnonymousType(content, new
                {
                    Id = default(Guid?),
                    ChainId = default(Guid?),
                    State = new
                    {
                        Status = default(MessageStatus),
                        Error = default(string)
                    },
                    // error
                    Error = default(string),
                    Message = default(string)
                });

                if (response.StatusCode == System.Net.HttpStatusCode.OK &&
                    result.Id != null)
                {
                    return result.State?.Status ?? MessageStatus.False;
                }
                else
                {
                    // log
                }
            }
            catch (Exception e)
            {
                // log
            }
            finally
            {
                client?.Dispose();
            }
            return MessageStatus.False;
        }

        private static async Task<Guid?> SendFile(string token)
        {
            var filePath = "C:\\Users\\User\\Desktop\\123.jpg";
            HttpClient client = null;
            try
            {
                var fileBytes = File.ReadAllBytes(filePath);

                var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/storage");
                request.Headers.Add("Authorization", token);
                request.Content = new ByteArrayContent(fileBytes);
                request.Content.Headers.Add("Content-Type", "image/jpeg");

                client = new HttpClient();
                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeAnonymousType(content, new
                {
                    Key = default(Guid?),
                    // error
                    Error = default(string),
                    Message = default(string)
                });

                if (response.StatusCode == System.Net.HttpStatusCode.OK &&
                    result.Key!= null)
                {
                    return result.Key;
                }
                else
                {
                    // log
                }
            }
            catch (Exception e)
            {
                // log
            }
            finally
            {
                client?.Dispose();
            }
            return null;
        }

        private static async Task<Guid?> SendViberBulk(string token, Guid imageId)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/sendings");
            request.Headers.Add("Authorization", token);
            var content = JsonConvert.SerializeObject(new
            {
                recipient = new[] { "+79999999999", "+79999999998" },
                type = "viber",
                payload = new
                {
                    // убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
                    sender = "X-City",
                    text = "Hello world!" + DateTime.UtcNow.ToString("o"),
                    image = imageId.ToString(),
                    button = new
                    {
                        text = "best sms service",
                        url = "https://sigmasms.ru/"
                    }
                },
                schedule = DateTime.UtcNow.AddMinutes(5).ToString("o"),
                fallbacks = new[]
                {
                    new Dictionary<string, object>
                    {
                        { "type", "viber" },
                        { "payload", new
                            {
                                // убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
                                sender = "X-City",
                                text = "Hello world!" + DateTime.UtcNow.AddMinutes(6).ToString("o"), // для уникальности сообщения
                                image = imageId.ToString(),
                                button = new
                                {
                                    text = "best sms service",
                                    url = "https://sigmasms.ru/"
                                }
                            }
                        },
                        { "$options", new
                            {
                                onTimeout = new
                                {
                                    timeout = 60,
                                    except = new[] { "seen" }
                                }
                            }
                        }
                    }
                }
            });
            request.Content = new StringContent(content, Encoding.UTF8, "application/json");

            HttpClient client = null;
            try
            {
                client = new HttpClient();
                var response = await client.SendAsync(request);
                var resonseContent = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeAnonymousType(resonseContent, new
                {   // минимальный набор параметров для получения ответа
                    groupId = default(Guid?),
                    Error = default(string)
                });

                if (response.StatusCode == System.Net.HttpStatusCode.OK &&
                    result.groupId != null)
                {
                    return result.groupId;
                }
                else
                {
                    // log
                }
            }
            catch (Exception e)
            {
                // log
            }
            finally
            {
                client?.Dispose();
            }
            return null;
        }

        private static async Task<bool> GetGroupStatus(string token, Guid groupId)
        {
            var builder = new UriBuilder("https://online.sigmasms.ru/api/sendings/");

            var parameters = HttpUtility.ParseQueryString("");
            parameters.Add("groupId", groupId.ToString());
            builder.Query = parameters.ToString();
            var request = new HttpRequestMessage(HttpMethod.Get, builder.Uri);
            request.Headers.Add("Authorization", token);

            HttpClient client = null;
            try
            {
                client = new HttpClient();
                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeAnonymousType(content, new
                {
                    Offset = default(int),
                    Limit = default(int),
                    Data = new[] {
                        new
                        {
                            Id = default(Guid?),
                            ChainId = default(Guid?),
                            GroupId = default(Guid?),
                            State = new
                            {
                                Status = default(MessageStatus),
                                Error = default(string)
                            }
                        }
                    },
                    // error
                    Error = default(string),
                    Message = default(string)
                });

                if (response.StatusCode == System.Net.HttpStatusCode.OK && 
                    string.IsNullOrEmpty(result.Error) &&
                    result.Data != null &&
                    result.Data.Any(i => string.IsNullOrEmpty(i.State.Error)))
                {
                    var successMessages = result.Data
                        .Where(i =>
                            i.State.Status == MessageStatus.delivered ||
                            i.State.Status == MessageStatus.sent ||
                            i.State.Status == MessageStatus.seen)
                        .Select(i => i.Id)
                        .ToList();

                    return true;
                }
                else
                {
                    // log
                }
            }
            catch (Exception e)
            {
                // log
            }
            finally
            {
                client?.Dispose();
            }
            return false;
        }
    }
}
